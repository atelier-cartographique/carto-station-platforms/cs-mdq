export { templates } from './template';
import { logoDataFR } from './logo-fr';
import { logoDataNL } from './logo-nl';
export * from './meta';

export const logoData = `${logoDataFR}##${logoDataNL}`
